# SVC를 위한 확장 프로그램

## 기능

### 파일복사

<p align="center">
  <br />
  <img src="https://gitlab.com/minamhunmin/svc/raw/master/resource/img/me_sample_copy.gif" alt="sample copy Preview" />
  
  <br />
</p>

- 복사를 윈하는 파일 우클릭해서 "샘플 만들기" 클릭
- 파일명 앞에 언더바가 있는 파일을 추출하여 두개 이상의 파일이 있다면 선택창에서 선택
- 입력창이뜨면 원하는 파일명을 삽입

### 스니팻

> strip
- 블록지정하여 우클릭으로도 가능

<p align="center">
  <br />
  <img src="https://gitlab.com/minamhunmin/svc/raw/master/resource/img/me1.gif" alt="strip Preview1" />
  
  <br />
</p>

```js
// <STRIP_WHEN_RELEASE,
var a = "여기는 배포시 삭제됩니다."
// STRIP_WHEN_RELEASE>
```

```html
<!--<STRIP_WHEN_RELEASE-->
<div>여기는 배포시 삭제됩니다.<div>
<!--STRIP_WHEN_RELEASE>-->
```