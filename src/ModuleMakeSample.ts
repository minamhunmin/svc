'use strict'

import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import Utils from './Utils';

export function ModuleMakeSample(selected: vscode.Uri) {
    const MSG_NOTFOUND_SAMPLE_PAGE  = "샘플이 존재하지 않습니다."
    const MSG_NEED_FILE_NAME = "원하는 파일이름을 입력해야합니다."

    ///////////////////////////////////////////////////
    // 타겟 폴더 찾기
    ///////////////////////////////////////////////////

    // let fileName = path.basename(selected.fsPath)
    let folderName: string = path.dirname(selected.fsPath)
    let isDirExistes: boolean = fs.existsSync(selected.fsPath) && fs.lstatSync(selected.fsPath).isDirectory();

    let targetFolder: string = isDirExistes ? selected.fsPath : folderName;

    let globalList:string[] = []

    let items:string[] = []
    items = walkSync(targetFolder)
    items = getUnderBarFileList(items);
    setList(items);


    items = getDupDeleteFileList(items);

    if(items.length == 1){
        makeFiles(items[0])
    }
    else if(items.length > 0){
        let options = {
            placeHolder: "복사하고 싶은 파일을 선택해 주세요."
        }

        vscode.window.showQuickPick(items, options).then(value => {
            if(!value){
                Utils.showErrorMsg(MSG_NOTFOUND_SAMPLE_PAGE);
                return;
            }

            makeFiles(value);
        })
    }
    else {
        Utils.showErrorMsg(MSG_NOTFOUND_SAMPLE_PAGE);
        return;
    }

    function walkSync(dir: string, fileList: string[] = []): string[] {
        
        fs.readdirSync(dir).forEach(file => {
            let dirFile:string = path.join(dir, file)
            try{
                fileList = walkSync(dirFile, fileList);
            }
            catch(err){
                if (err.code === 'ENOTDIR' || err.code === 'EBUSY') fileList = [...fileList, dirFile];
                else throw err;
            }
        })
        return fileList;
    }
    
    function getUnderBarFileList(list:string[], fileList:string[] = []){
        list.forEach(fileObj => {
            let fileName = path.basename(fileObj)
            if(fileName.substr(0,1) == '_'){
                fileList.push(fileObj)
            }
        })
        return fileList
    }

    function setList(list:string[]){
        globalList = list;
    }
    
    function getList(){
        return globalList
    }
    
    function makeFiles(targetFileName:string) {
        ///////////////////////////////////////////////////
        // 선택한 파일 찾기
        ///////////////////////////////////////////////////
    
        let oldList = getList();
        let newList = oldList.filter(file => {
            return file.indexOf(targetFileName) > -1;
        })
    
        let options = {
            placeHolder: '원하는 파일이름'
        }
    
        vscode.window.showInputBox(options).then(value => {
            // 취소
            if( value === undefined){
                return;
            }
    
            // 입력값 없이
            if(!value){
                Utils.showErrorMsg(MSG_NEED_FILE_NAME);
                makeFiles(targetFileName);
                return;
            }
    
            // 
    
            newList.forEach(file => {
                let fileNameObj = path.parse(file);
                let newDir = fileNameObj.dir;
                let newExt = fileNameObj.ext;
                let newName = value + newExt;
                let newFileName = path.join(newDir, newName);
    
                fs.copyFile(file, newFileName, (err) => {
                    if (err) throw err
                })
            })


            Utils.showErrorMsg(value + ' 생성 완료!');
        })
    
    }
    
    
    function getDupDeleteFileList(list:string[], fileList:string[] = []) {
        list.forEach(fileObj => {
            let fileName = path.parse(fileObj).name;
            if(fileList.indexOf(fileName) > -1) return true;
            
            fileList.push(fileName);
        })
    
        return fileList;
    }
}