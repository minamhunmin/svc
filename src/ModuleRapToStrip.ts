'use strict'

import * as vscode from 'vscode';
import Utils from './Utils';

export function ModuleRapToStrip() {

    const STRIP_WHEN_RELEASE = "STRIP_WHEN_RELEASE"

    ///////////////////////////////////////////////////
    // 타겟 폴더 찾기
    ///////////////////////////////////////////////////
    wrapSelection("바보");

    function wrapSelection(target: string ) {
        if (!target) {
            return;
        }
    
        const editor = vscode.window.activeTextEditor;
        if(!editor) return;

        const {document, selections} = editor;
        
    
        editor.edit((b) => {
            selections.forEach((selection) => {
                if (!selection.isEmpty) {
                    
                    const text = document.getText(selection);
                    
                    // STRIP_WHEN_RELEASE 이게있으면 리턴
                    if (text.includes(STRIP_WHEN_RELEASE)) {
                        showErrorMsg(STRIP_WHEN_RELEASE + ' 이미 존재합니다.')
                        return;
                    }
    
                    b.replace(selection, Utils.wrap(document.languageId, text, STRIP_WHEN_RELEASE));
                }
            });
        });
    };

    
    
    function showErrorMsg(msg: string){
        vscode.window.showInformationMessage(msg);
    }

}
