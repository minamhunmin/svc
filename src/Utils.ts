'use strict'

import * as vscode from 'vscode';

export default class Utils {
    
    static wrap(type: string, text: string, target: string): string {
        let result = text;
    
        // 기본
        let start = "\t// <" + target;
        let end = "\t// " + target + ">";
    
        switch(type){
            case "javascript":
                start = "\t// <" + target;
                end = "\t// " + target + ">";
            break;
            case "html":
                start = "<!--<" + target + "-->"
                end = "<!--" + target + ">-->"
            break;
        }
    
        result = start + '\n' + text + '\n' + end;
        
        return result;
    }

    static showErrorMsg(msg:string){
        vscode.window.showInformationMessage(msg);
    }
}