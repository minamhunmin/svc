'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// import * as ModuleMakeSample from './ModuleMakeSample';
// import * as ModuleRapToStrip from './ModuleRapToStrip';
import { ModuleRapToStrip } from './ModuleRapToStrip';
import { ModuleMakeSample } from './ModuleMakeSample';
import { SVCIntellisense } from './SVCIntellisense';


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    context.subscriptions.push(vscode.commands.registerCommand('extension.rapToStrip', ModuleRapToStrip));
    context.subscriptions.push(vscode.commands.registerCommand('extension.makeSample', ModuleMakeSample));

    const provider = new SVCIntellisense();
    const triggers = ['/','.'];
    const selector = ['typescript', 'javascript', 'javascriptreact', 'typescriptreact', 'js'];
    context.subscriptions.push(vscode.languages.registerCompletionItemProvider(selector, provider, ...triggers));
}

// this method is called when your extension is deactivated
export function deactivate() {
}